require_relative 'csv_reader'
require_relative 'helpers'

reader = CsvReader.new
    print_memory_usage do
        print_time_spent do
            ARGV.each do |csv_file_name|
            STDERR.puts "Processing #{csv_file_name} \n "
                reader.read_in_csv_data(csv_file_name)
            end

            puts "----------------- Detalle -----------------"
            puts "Total value in stock = $#{reader.total_value_in_stock} \n "

            res = reader.number_of_each_isbn

            puts "Number of each isbn:" 
                res.each do |k, v|
                    puts "ISBN: #{k}  Cantidad: #{v}"
                end
            puts "--------------------------------------------"    
        end
end
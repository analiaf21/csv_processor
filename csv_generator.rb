require 'csv'

class CsvGenerator
    def initialize(number)
        @rows_number = number * 200000
        create_csv
    end

    def create_csv
        CSV.open('archivo.csv', 'w') do |csv|
            csv << ["Date", "ISBN", "Amount"]
            1..@rows_number.times do 
                csv << ["2008-04-12","978-1-9343561-0-4", 39.45]
                csv << ["2008-05-10","978-1-9343561-0-5", 11.24]
                csv << ["2008-01-09","978-1-9343561-0-6", 12,50]
                csv << ["2008-04-12","978-1-9343561-0-4", 39.45]
                csv << ["2008-01-09","978-1-9343561-0-6", 12,50]
            end
        end
    end
end
require 'csv'

class CsvReader
    attr_reader :total_value_in_stock, :number_of_each_isbn
    def initialize
        @total_value_in_stock = 0
        @number_of_each_isbn = Hash.new(0)
    end

    def read_in_csv_data(csv_file_name)
        CSV.foreach(csv_file_name, headers: true) do |row|
            @total_value_in_stock = @total_value_in_stock + Float(row["Amount"])
            @number_of_each_isbn[row['ISBN']] =  @number_of_each_isbn[row['ISBN']] += 1
        end
    end

end
